import 'package:flutter/material.dart';


import 'virtual_tools.dart';
// import 'package:virtual_tools/presentation/quiz.dart';
// import 'presentation/splash_screen.dart';
// import 'presentation/login.dart';
// import 'package:flutter/services.dart';
// import 'package:virtual_tools/presentation/home.dart';
// import 'package:virtual_tools/presentation/product_details.dart';
// import 'package:virtual_tools/presentation/tour.dart';
// import 'res/colors.dart';

void main() {
  runApp(new RestartWidget(
    child: VirtualTools(),
  ));
}

class RestartWidget extends StatefulWidget {
  final Widget child;

  const RestartWidget({Key key, this.child}) : super(key: key);

  static restartApp(BuildContext context) {
    final _RestartWidgetState state =
    context.ancestorStateOfType(const TypeMatcher<_RestartWidgetState>());
    state.restartApp();
  }

  @override
  _RestartWidgetState createState() => new _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = new UniqueKey();

  void restartApp() {
    this.setState(() {
      key = new UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    
    return new Container(
      key: key,
      child: widget.child,
    );
  }
}