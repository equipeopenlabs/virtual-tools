import 'package:flutter/material.dart';
import 'colors.dart';
import 'package:shared_preferences/shared_preferences.dart';



class GlobalTheme{
  GlobalTheme._();
  static final GlobalTheme state = GlobalTheme._();

  String operadora = "init";
  ThemeData themeData;
  AtributeTheme atributeTheme;

  Future<bool> getOperadora() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    operadora =
    ((prefs.getString('operadoraGet') == null) ? "OpenLabs" : prefs.getString(
        'operadoraGet'));
    initGlobalTheme();
    return true;
  }

  void initGlobalTheme(){

    switch (operadora) {
      case "TIM":
        atributeTheme = AtributeTheme(
            AppColors.timBlue,
            AppColors.timCyan,
            'assets/images/logo_tim.png',
          'assets/images/white_Bg.png',
          'assets/images/tim-avatar.png');//tim_login.jpg
        break;

      case "OI":
        atributeTheme = AtributeTheme(
            AppColors.oiYellow,
            AppColors.oiPurple,
            'assets/images/oi.png',
            'assets/images/white_Bg.png',
            'assets/images/oi-avatar.png');//oi_login.jpg
        break;

      case "VIVO":
        atributeTheme = AtributeTheme(
            AppColors.vivoPurple,
            AppColors.vivoOrange,
            'assets/images/vivo.png',
            'assets/images/white_Bg.png',
            'assets/images/vivo-avatar.png',);//vivo_login.png
        break;
      default:
        atributeTheme = AtributeTheme(
            AppColors.lightGreen,
            AppColors.cyan,
            'assets/images/logo-circle-open-labs.png',
            'assets/images/login-bg-open-labs.jpg',
            'assets/images/logo-circle-open-labs.png');
    }
    themeData = ThemeData(
      appBarTheme: AppBarTheme(iconTheme: IconThemeData(color: Colors.white)),
      //primarySwatch: atributeTheme.primaryColor,
      primaryColor: atributeTheme.primaryColor,
      accentColor: atributeTheme.secondaryColor,
      fontFamily: 'Dosis',
      //backgroundColor: Colors.teal,
      buttonColor: atributeTheme.primaryColor,
      //primaryColorBrightness: Brightness.dark,
      cursorColor: atributeTheme.primaryColor,
      textTheme: Typography.blackCupertino,
      /*
      primaryTextTheme: TextTheme(
        body1: TextStyle(
        color: Colors.red
          ),
        ),
      accentTextTheme: TextTheme(
        body1: TextStyle(
          color: Colors.redAccent
        ),
      ),*/
    );

  }
}


class AtributeTheme{
  //Cores
  final Color primaryColor;
  final Color secondaryColor;

  //Image
  final String splashImagePath;
  final String loginImagePath;

  final String avatarLogo;

  AtributeTheme(this.primaryColor, this.secondaryColor, this.splashImagePath, this.loginImagePath, this.avatarLogo);

}