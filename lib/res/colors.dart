import 'package:flutter/material.dart';

class AppColors{
  static Color lightGreen = Color(0xFF0099ab);
  static Color dartGreen = Color(0xFF028696);
  static Color cyan = Color(0xFF1F889D);

  //Cores Oi Telefonica
  static Color oiPurple = Color(0xFF8F25E5);
  static Color oiYellow = Color(0xFFFFD700);

  //Cores Tim
  static Color timBlue = Color(0xFF00508C);
  static Color timCyan = Color(0xFF1F889D);

  //Cores Vivo
  static Color vivoPurple = Color(0xFf993399);
  static Color vivoLightGreen = Color(0xFF8DC744);
  static Color vivoOrange = Color(0xFFFF9900);

}


class ColorsApplication{

  static final String light_green = "0099ab";
  static final String dart_green = "028696";
  static final String cyan = "1F889D";
  static final String orange = "F5F5F5";
  static final String light_grey = "#F8F8F8";

  static Color formatColorBaseLightGrey(){
    return new Color(hexToInt(light_grey));
  }
  static Color formatColorBaseOrange(){
    return new Color(hexToInt(orange));
  }

  static Color formatColorBaseCyan(){
    return new Color(hexToInt(cyan));
  }

  static Color formatColorBase(){
    return new Color(hexToInt(light_green));
  }

  static Color formatColorBaseDartGreen(){
    return new Color(hexToInt(dart_green));
  }

  static int hexToInt(String colorStr)
  {
    colorStr = "FF" + colorStr;
    colorStr = colorStr.replaceAll("#", "");
    int val = 0;
    int len = colorStr.length;
    for (int i = 0; i < len; i++) {
      int hexDigit = colorStr.codeUnitAt(i);
      if (hexDigit >= 48 && hexDigit <= 57) {
        val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 65 && hexDigit <= 70) {
        // A..F
        val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 97 && hexDigit <= 102) {
        // a..f
        val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
      } else {
        throw new FormatException("An error occurred when converting a color");
      }
    }
    return val;
  }
}