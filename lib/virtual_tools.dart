import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/quiz.dart';
import 'package:virtual_tools/presentation/success.dart';
import 'presentation/splash_screen.dart';
import 'presentation/login.dart';
import 'package:flutter/services.dart';
import 'package:virtual_tools/presentation/home.dart';
import 'package:virtual_tools/presentation/product_details.dart';
import 'package:virtual_tools/presentation/tour.dart';
import 'res/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'res/global_theme.dart';
import 'package:path_provider/path_provider.dart';


class VirtualTools extends StatelessWidget {
  final GlobalTheme globalTheme = GlobalTheme.state;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return FutureBuilder<bool>(
        future: globalTheme.getOperadora(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return MaterialApp(
              routes: <String, WidgetBuilder>{
                '/SplashScreen': (BuildContext context) => SplashScreen(),
                '/Tour': (BuildContext context) => Tour(),
                '/Login': (BuildContext context) =>
                    Login(
                      imagemLogin: globalTheme.atributeTheme.loginImagePath,
                      imagemLogo: globalTheme.atributeTheme.splashImagePath,),
                '/Home': (BuildContext context) => Home(
                      drawerImage: globalTheme.atributeTheme.splashImagePath,
                      colorLeft: globalTheme.atributeTheme.primaryColor,
                      colorRight: globalTheme.atributeTheme.secondaryColor,
                    ),
                '/ProductDetails': (BuildContext context) => ProductDetails(),
                '/Quiz': (BuildContext context) => Quiz(),
                '/Sucess': (BuildContext context) => Sucess()
              },
              theme: globalTheme.themeData,
              /*
              ThemeData(
                accentColor: globalTheme.atributeTheme.secondaryColor,
                primaryColor: globalTheme.atributeTheme.primaryColor,

                fontFamily: 'Dosis',
                primaryColorBrightness: Brightness.dark,
                cursorColor: ColorsApplication.formatColorBase(),
                textTheme: new TextTheme(
                  body1: new TextStyle(
                      color: ColorsApplication.formatColorBase()),
                ),
              ),*/
              home: new SplashScreen(
                imagemSplash: globalTheme.atributeTheme.splashImagePath,
              ),
            );
          }
          return CircularProgressIndicator();
        });
  }
}