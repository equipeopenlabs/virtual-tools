import 'dart:io';
import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/component/avatar_picture.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:virtual_tools/main.dart';

class ThirdFragment extends StatefulWidget {
  @override
  _ThirdFragmentState createState() => _ThirdFragmentState();
}

class _ThirdFragmentState extends State<ThirdFragment> {

  double cardHeight;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    cardHeight =  MediaQuery.of(context).size.height;

    return Container(
        color: ColorsApplication.formatColorBaseLightGrey(),
//      height: double.infinity,
//      width: double.infinity,
//     height: double.maxFinite,

        child: Column(
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Container(
                  height: 350,
                  color: ColorsApplication.formatColorBaseLightGrey(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: AvatarPicture()),
                      new Padding(
                        padding: new EdgeInsets.all(4.0),
                        child: new Text('User Name',
                            style: TextStyle(
                                color: ColorsApplication.formatColorBaseCyan(),
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold)),
                      ),
                    ],
                  )),
            ),
            Flexible(
              flex: 3,
              child: Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                color: ColorsApplication.formatColorBaseLightGrey(),
                child: CustomScrollView(slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        createTextCard('TOUR', 'assets/images/ic-tour.png', '/Tour', context),
                        createTextCard('PESQUISA', 'assets/images/ic-survey.png', '/Quiz', context),
                        createTextCard("SAIR", 'assets/images/ic-logout.png', '/Login', context),
                        Container(
                            //  height: 10,
                            // color: Colors.yellow,
                            alignment: Alignment.bottomCenter,
                            child: Column(children: <Widget>[
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  createProviderCard('Open', 'assets/images/logo-circle-open-labs.png', context),
                                  createProviderCard('VIVO', 'assets/images/vivo.png', context),
                                  createProviderCard('OI', 'assets/images/oi.png', context),
                                  createProviderCard('TIM', 'assets/images/tim1.png', context),
                                ],
                              ),
                              Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(bottom: 5),
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Text(
                                    "v1.0",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                  ),
                                ),
                              )
                            ]))
                      ],
                    ),
                  ),
                ]),
              ),
            ),
          ],
        ));
  }

  InkWell createTextCard(String text, String imagePath, String naviagatorPath, BuildContext context) {
    Color themeColor = Theme.of(context).primaryColor;
    return InkWell(
      onTap: () {
        if (naviagatorPath != '')
          if (naviagatorPath != '/Login')
            Navigator.pushNamed(context, naviagatorPath);
          else
            Navigator.pushReplacementNamed(context, naviagatorPath);
      },
      child: Card(
          color: themeColor.withOpacity(0.7) ,
          elevation: 4.0,
          child: Container(
            margin: EdgeInsets.only(left: 10),
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Image.asset(imagePath, width: 25, height: 25),
                ),
                Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(left: 15),
                      child: Text(text,
                          textAlign: TextAlign.left,
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
                    ))
              ],
            ),
          )),
    );
  }

  InkWell createProviderCard(String stringOperador, String path, BuildContext context) {
    Color themeColor = Theme.of(context).primaryColor;
    //TODO: Tirar isso daqui para sempre
    _getOperadora(String operadora) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('operadoraGet', operadora);
      RestartWidget.restartApp(context);
    }

    return InkWell(
      onTap: () {
        _getOperadora(stringOperador);
      },
      child: Card(

          color: themeColor.withOpacity(0.7),
          elevation: 4.0,
          child: Container(
              height:  cardHeight*0.11,
              width:  cardHeight*0.11,
              child: new Padding(
                padding: new EdgeInsets.all(5),
                child: new Image.asset(path, ),
              ))),
    );
  }
}
