import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/item/itens_product_local.dart';
import 'package:virtual_tools/presentation/item/itens_grid_local.dart';
import 'package:virtual_tools/res/colors.dart';

class SecondFragment extends StatefulWidget {
  @override
  _SecondFragment createState() => _SecondFragment();
}

class _SecondFragment extends State<SecondFragment> {
  int pos = 0;
  ScrollController _scrollViewController;
  PageController controller;


  callBack(pos) {
    setState(() {
      controller.animateToPage(pos,
          duration: Duration(milliseconds: 1000), curve: Curves.easeIn);
      this.pos = pos;
    });
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    controller.dispose();
    super.dispose();
  }

    @override
  void initState() {
    _scrollViewController = new ScrollController();
    controller = PageController(
      initialPage: 0,
    );

    //grid = meuGrid();
    //controller = PageController(initialPage: 0);
    super.initState();
  }

  ThemeData theme;
  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    ItemGridLocal();

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            theme.primaryColor.withOpacity(1.0),
            theme.primaryColor.withOpacity(0.8),
            theme.primaryColor.withOpacity(0.5),
            theme.primaryColor.withOpacity(0.2),
          ],
        ),
      ),
      child: NestedScrollView(
          controller: _scrollViewController,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [ItensProductLocal(pos, callBack)],
                ),
              ),
            ];
          },
          body: PageView.builder(
            physics: NeverScrollableScrollPhysics(),
            controller: controller,
            itemCount: ItemGridLocal.grid.length,
            itemBuilder: (BuildContext context, int index) {
              return ItemGridLocal.grid[index];
            },
          ),
        ));
  }


//    return Container(
//     //   height: double.maxFinite,
//        decoration: BoxDecoration(
//          gradient: LinearGradient(
//            begin: Alignment.topRight,
//            end: Alignment.bottomLeft,
//            stops: [0.1, 0.5, 0.7, 0.9],
//            colors: [
//              // Colors are easy thanks to Flutter's Colors class.
//              Colors.teal[200],
//              Colors.teal[100],
//              Colors.teal[50],
//              Colors.teal[50],
//            ],
//          ),
//        ),

  }
