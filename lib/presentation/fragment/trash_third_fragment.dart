import 'package:flutter/material.dart';
import 'package:virtual_tools/res/colors.dart';

class ThirdFragment extends StatelessWidget {

  BuildContext context;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    this.context = context;

    return Container(
      margin: const EdgeInsets.only(left: 10),
      child: Column(

        children: <Widget>[
          Expanded(
            flex: 3,
            child: GestureDetector(
              child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Padding(
                        padding: new EdgeInsets.only(left: 1.0),
                        child: new Image.asset('assets/images/avatar1.png', fit: BoxFit.fill, height: 60, width: 60,),
                      ),
                      new Padding(
                        padding: new EdgeInsets.all(2.0),
                        child: new Text('Dennys Barbosa', style: TextStyle(color: ColorsApplication.formatColorBaseCyan(), fontSize: 30.0, fontWeight: FontWeight.bold) ),
                      ),
                    ],
                  )
              ),
            ),
          ),

          Expanded(
            flex: 4,
            child: Container(
              padding: EdgeInsets.only(left: 10.0, right: 15.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          child: Text("Tour", textAlign: TextAlign.start, style: TextStyle(color: ColorsApplication.formatColorBase(), fontSize: 17, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      Align(
                          alignment: Alignment.topRight,
                          child: new Container(
                            height: 25,
                            width: 25,
                            child: new Icon(
                              Icons.arrow_forward_ios,
                              color: ColorsApplication.formatColorBase(),
                            ),
                          )
                      ),
                    ],
                  )
                  ,Align(
                    child: Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 15.0, top: 10.0),
                      height: 2,
                      color: ColorsApplication.formatColorBase(),
                    ),
                  ),
                  Row(
                    children: <Widget>[

                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(top: 10),
                          child: Text("Tour", textAlign: TextAlign.start, style: TextStyle(color: ColorsApplication.formatColorBase(), fontSize: 17, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      Align(
                          alignment: Alignment.topRight,
                          child: new Container(
                            padding: EdgeInsets.only(top: 5),
                            height: 25,
                            width: 25,
                            child: new Icon(
                              Icons.arrow_forward_ios,
                              color: ColorsApplication.formatColorBase(),
                            ),
                          )
                      ),
                    ],
                  ),
                  Align(
                    child: Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 15.0, top: 10.0),
                      height: 2,
                      color: ColorsApplication.formatColorBase(),
                    ),
                  ),
                  Row(
                    children: <Widget>[

                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(top: 10),
                          child: Text("Tour", textAlign: TextAlign.start, style: TextStyle(color: ColorsApplication.formatColorBase(), fontSize: 17, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      Align(
                          alignment: Alignment.topRight,
                          child: new Container(
                            padding: EdgeInsets.only(top: 5),
                            height: 25,
                            width: 25,
                            child: new Icon(
                              Icons.arrow_forward_ios,
                              color: ColorsApplication.formatColorBase(),
                            ),
                          )
                      ),
                    ],
                  )
                  ,Align(
                    child: Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 15.0, top: 10.0),
                      height: 2,
                      color: ColorsApplication.formatColorBase(),
                    ),
                  ),

                  Row(
                    children: <Widget>[

                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(top: 10),
                          child: Text("Tour", textAlign: TextAlign.start, style: TextStyle(color: ColorsApplication.formatColorBase(), fontSize: 17, fontWeight: FontWeight.bold),),
                        ),
                      ),
                      Align(
                          alignment: Alignment.topRight,
                          child: new Container(
                            padding: EdgeInsets.only(top: 5),
                            height: 25,
                            width: 25,
                            child: new Icon(
                              Icons.arrow_forward_ios,
                              color: ColorsApplication.formatColorBase(),
                            ),
                          )
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              child: Column(

                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Text("Teste de Interno", textAlign: TextAlign.start, style: TextStyle(color: ColorsApplication.formatColorBase()),),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Image.asset('assets/images/oi.png', fit: BoxFit.fill, height: 40, width: 40,)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Image.asset('assets/images/vivo.png', fit: BoxFit.fill, height: 30, width: 60,),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Image.asset('assets/images/logo_tim.png', fit: BoxFit.fill, height: 30, width: 50),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: EdgeInsets.all(5),
              height: 20,
              child: Text("v1.0", textAlign: TextAlign.start, style: TextStyle(color: ColorsApplication.formatColorBase(), fontSize: 10),),
            ),
          )
        ],
      ),
    );
  }
}