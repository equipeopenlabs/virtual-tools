import 'package:flutter/material.dart';

class ItensProductLocal extends StatelessWidget {
  final int pos;
  final Function(void) callBack;

  ItensProductLocal(this.pos, this.callBack);


  List<Widget> ListMyWidgets() {
    List<Widget> list = new List();
    for (int i = 0; i < 2; i++) {
      list.add(new ItemProduto("bla bla",i,pos == i,callBack));
    }

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: ListMyWidgets(),
      ),
    );
  }
}

class ItemProduto extends StatelessWidget {
  final String text;
  final bool selected;
  final int pos;
  final Function(void) callBack;

  ItemProduto(this.text,this.pos ,this.selected, this.callBack); //  final String image_location;

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 120,
      width: 120,
      child: InkWell(
        onTap: () {
          callBack(pos);
        },
        child: Stack(
          alignment: Alignment.lerp(Alignment.topCenter, Alignment.center, .40),
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.circular(40.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black.withOpacity(1),
                      offset: Offset(2, 2),
                      blurRadius: 2,
                    )
                  ],
                  border: selected?  new Border.all(color: Colors.orange, width: 2) :new Border.all(color: Colors.orange, width: 0) ),
              child: new Image.asset('assets/images/android.png',
                  width: 80, height: 80),
            ),
            Align(
              alignment: Alignment.lerp(
                  Alignment.topCenter, Alignment.bottomRight, 0.61),
                  child: this.pos == 0 ? Image.asset("assets/images/ic-ok-green.png", width: 20, height: 20,) : Image.asset("assets/images/ic-cancel-red.png", width: 20, height: 20,),
//              child: CircleAvatar(
//                backgroundColor: Colors.transparent,
//                foregroundColor: selected ? Colors.black : Colors.black54,
//                child:
//              ),
            ),
            Align(
                alignment: Alignment.lerp(
                    Alignment.center, Alignment.bottomCenter, 0.9),
                child: Text(
                   _manageProductName(this.pos),
                  style: TextStyle(fontWeight: selected ? FontWeight.w900 : FontWeight.w500, color: Colors.white),
                )),
          ],
        ),
      ),
    );
  }
  
  String _manageProductName(int pos){
    
    switch(pos){
      
      case 0:
        return "ACESSO";
     
      case 1:
        return "CPE/HN";
    }
  }
}
