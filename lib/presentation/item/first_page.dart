import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:virtual_tools/presentation/splash_screen.dart';
import 'package:virtual_tools/presentation/home.dart';
import 'package:virtual_tools/presentation/login.dart';

String operadora = "init";
Color cor;
String imagem;

Future<String> _getOperadora() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  operadora =
  ((prefs.getString('operadoraGet') == null) ? "OpenLabs" : prefs.getString(
      'operadoraGet'));
  return operadora;
}


class InitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: _getOperadora(),

      builder: (context, snapshot) {
        if (snapshot.hasData) {

          switch (operadora) {
            case "TIM":
              cor = Colors.red;
              imagem = 'lib/resources/logo_tim.png';
              break;

            case "OI":
              cor = Colors.yellow;
              imagem = 'lib/resources/oi.png';
              break;

            case "VIVO":
              cor = Colors.purple;
              imagem = 'lib/resources/vivo.png';
              break;

            default:
              cor = Colors.blue;
              imagem = 'lib/resources/image_splash.png';
          }


          return MaterialApp(

            theme: ThemeData(
                primarySwatch: cor),

            routes: <String, WidgetBuilder>{
              '\Home': (BuildContext context) => Home(),
              '\SplashScreen': (BuildContext context) => SplashScreen(),
              '\Login': (BuildContext context) => Login(),
            },

            home: SplashScreen(imagemSplash: imagem,),

          );
        }

        return CircularProgressIndicator();
      },
    );
  }
}