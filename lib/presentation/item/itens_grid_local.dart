import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ItemGridLocal extends StatelessWidget {

  BuildContext context;
  static int position = 4;
  static int product = 3;
  static List<Widget> grid = List<Widget>();
  
  ItemGridLocal(){
    if(grid.length == 0) {
      initList();
    }
  }

  void initList(){

   var tempGrid;
    for (int i = 0; i < product; i++) {
      tempGrid = GridView.builder(
        itemCount: position + i * 2,
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return ItemGridLocal();
        },
      );
      ItemGridLocal.grid.add(tempGrid);
    }
  }

  @override
  Widget build(BuildContext context) {

    this.context = context;
    return new InkWell(
        onTap: () {
          _goItemDetails();
        },
        child: Card(
            color: Colors.white,
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: Padding(
                padding: const EdgeInsets.all(1),
                child: Container(
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                            flex: 3,
                            child: ClipRRect(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(4),
                                    topRight: Radius.circular(4)),
                                child: Image.asset(
                                    "assets/images/image-tecnologia.jpg",
                                    fit: BoxFit.cover))),
                        Expanded(
                            flex: 1,
                            child: new Container(
                                alignment: Alignment.center,
                                child: Text("Teste",
                                    style: TextStyle(
                                        fontFamily: "fonts/dosis-regular.ttf",
                                        fontWeight: FontWeight.bold))
                                )),
                      ],
                    )
                )
            )
        )
    );
  }

  void _goItemDetails() {
    Navigator.pushNamed(context, '/ProductDetails');
  }
}
