import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/generic.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/presentation/fragment/first_fragment.dart';
import 'package:virtual_tools/presentation/fragment/second_fragment.dart';
import 'package:virtual_tools/presentation/fragment/third_fragment.dart';
//import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:virtual_tools/presentation/component/bottom_navy_bar.dart';
import 'component/home_drawer.dart';


class Home extends StatefulWidget {
  final String drawerImage;
  final Color colorLeft;
  final Color colorRight;

  const Home({Key key, this.drawerImage, this.colorLeft, this.colorRight}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new HomeState();
  }
}

class HomeState extends State<Home> with Generic{

  GlobalKey<ScaffoldState> _keyScaffold = GlobalKey<ScaffoldState>();

  PageController controller;

  @override
  void initState() {
    controller = PageController(initialPage: 0,);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {

    List<Widget> pages = [

      FirstFragment(),
      SecondFragment(),
      ThirdFragment(),
    ];


    return Scaffold(
      key: _keyScaffold,
      resizeToAvoidBottomPadding: false,

      // endDrawer: HomeDrawer(
      //   drawerImage: widget.drawerImage,
      //   colorRight: widget.colorRight,
      //   colorLeft: widget.colorLeft,
      //   onPressed: (indexDrawer){
      //     controller.animateToPage(indexDrawer, duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      //     _keyScaffold.currentState.openDrawer();
      //     },
      //),

      appBar: commonAppBar("Open Labs"),

      body: PageView.builder(
        physics: NeverScrollableScrollPhysics(),
        controller: controller,
        itemCount: pages.length,
        itemBuilder: (BuildContext context, int index){
        return pages[index];
        },
      ),


      bottomNavigationBar: AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context, Widget child){
          return BottomNavyBar(
            selectedIndex: (controller.page == null) ? 0 : controller.page.round(),
            onItemSelected: (index){
              controller.animateToPage(index, duration: Duration(milliseconds: 300), curve: Curves.easeIn);
            },
            items: [
              BottomNavyBarItem(
                icon: Icon(Icons.settings),
                title: Text('TOOLS'),
                activeColor: Theme.of(context).primaryColor,
              ),
              BottomNavyBarItem(
                  icon: Icon(Icons.home),
                  title: Text('LAN'),
                  activeColor: Theme.of(context).primaryColor
              ),
              BottomNavyBarItem(
                  icon: Icon(Icons.people),
                  title: Text('PROFILE'),
                  activeColor: Theme.of(context).primaryColor
              )
            ],
          );
        },
      ),
    );
  }
}
