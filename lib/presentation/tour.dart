import 'dart:math';

import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/animation/dots_indicator.dart';

class Tour extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new TourState();
  }
}

class TourState extends State<Tour>{

  final _controller = new PageController();
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  final List<Widget> _pages = <Widget>[
    new ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: new FlutterLogo(colors: Colors.blue),
    ),
    new ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: new FlutterLogo(style: FlutterLogoStyle.stacked, colors: Colors.red),
    ),
    new ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: new FlutterLogo(style: FlutterLogoStyle.horizontal, colors: Colors.green),
    ),
  ];


    @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Container(
            color: Colors.greenAccent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                  Expanded(
                    child: Container(
                    alignment: Alignment.bottomCenter,
                    color: Colors.greenAccent,
                    child:   new PageView.builder(
                     // physics: new AlwaysScrollableScrollPhysics(),
                      itemCount: _pages.length,
                      controller: _controller,
                      itemBuilder: (BuildContext context, int index) {
                        return _pages[index];
                      },
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 80,
                    width: double.infinity,
                    color: Colors.greenAccent,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            color: Colors.greenAccent,
                            child:  new DotsIndicator(
                              controller: _controller,
                              itemCount: _pages.length,
                              onPageSelected: (int page) {
                                _controller.animateToPage(
                                  page,
                                  duration: _kDuration,
                                  curve: _kCurve,
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                              child:Container(
                                alignment: Alignment.center,
                                height: 100,
                                width: 100,
                                color: Colors.greenAccent,
                                child: Text("PULAR",  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
                              ),
                            onTap: _goLogin,
                          ),
                        ],
                      )
                    )
                  ),
                ),
              ],
            )
        )
    );

  }

  void _goLogin(){
    Navigator.pushNamedAndRemoveUntil(context, '/Home', (Route<dynamic> route)=> false);
  }
}
