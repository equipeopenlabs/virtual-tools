import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/component/dialog.dart';

abstract class CommonDialogBuilder {
  final double dialogHeight = 256;

  final double topBarHeight = 8 * 256 / 32;
  final double titleHeight = 5 * 256 / 32;
  final double contentHeight = 12 * 256 / 32;
  final double actionsHeight = 7 * 256 / 32;

  Widget makeTopBarWidget();
  Widget makeTitleWidget();
  Widget makeContentWidget();
  Widget makeActionsWidget();

}

class SuccessDialogBuilder extends CommonDialogBuilder {
  final Color _topBarColor = Colors.lightGreen[300];
  final IconData iconData = IconData(59693, fontFamily: 'MaterialIcons');

  final double _contentHorizontalPadding = 24.0;

  final double _actionsMargin = 16.0;

  final String _title = 'Sucesso!';

  @override
  Widget makeTopBarWidget() {
    return Container(
        height: topBarHeight,
        constraints: BoxConstraints(minWidth: 300),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
            color: _topBarColor),
        child: Icon(
          iconData,
          color: Colors.white,
          size:
          40, //THIS SHOULD NOT BE HARDCODED... CHANGE THIS TO SOMETHING MORE ADAPTIVE.
        ));
  }

  @override
  Widget makeTitleWidget() {
    return Container(
      height: titleHeight,
      child: Center(
        child: Text(_title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize:
                18.0 // THIS SHOULD NOT BE HARDCODED... CHANGE THIS TO SOMETHING MORE ADAPTIVE.
            )),
      ),
    );
  }

  @override
  Widget makeContentWidget() {
    return Container(
      height: contentHeight,
      padding: EdgeInsets.symmetric(horizontal: _contentHorizontalPadding),
      child: Center(
          child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae tellus aliquam, interdum')), //  O TEXTO AQUI DEVE SER PASSADO ?
    );
  }

  @override
  Widget makeActionsWidget() {
    return Container(
        height: actionsHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0)
          ),
        ),

        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: _topBarColor),
            borderRadius: BorderRadius.circular(8.0),
          ),
          margin: EdgeInsets.only(
            left: _actionsMargin,
            right: _actionsMargin,
            bottom: _actionsMargin,
          ),
          child: Center(
            child: Text(
              'OK',
              style: TextStyle(
                  fontSize: 18.0
              ),
            ),
          ),
        )
    );
  }
}






class WarningDialogBuilder extends CommonDialogBuilder {
  final Color _topBarColor = Colors.lightBlue[200];
  final IconData iconData = IconData(57346, fontFamily: 'MaterialIcons');

  final double _contentHorizontalPadding = 24.0;

  final double _actionsMargin = 16.0;

  final String _title = 'Aviso!';

  @override
  Widget makeTopBarWidget() {
    return Container(
        height: topBarHeight,
        constraints: BoxConstraints(minWidth: 300),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
            color: _topBarColor),
        child: Icon(
          iconData,
          color: Colors.white,
          size:
          40, //THIS SHOULD NOT BE HARDCODED... CHANGE THIS TO SOMETHING MORE ADAPTIVE.
        ));
  }

  @override
  Widget makeTitleWidget() {
    return Container(
      height: titleHeight,
      child: Center(
        child: Text(_title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize:
                18.0 // THIS SHOULD NOT BE HARDCODED... CHANGE THIS TO SOMETHING MORE ADAPTIVE.
            )),
      ),
    );
  }

  @override
  Widget makeContentWidget() {
    return Container(
      height: contentHeight,
      padding: EdgeInsets.symmetric(horizontal: _contentHorizontalPadding),
      child: Center(
          child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vitae tellus aliquam, interdum')), //  O TEXTO AQUI DEVE SER PASSADO ?
    );
  }

  @override
  Widget makeActionsWidget() {
    return Container(
        height: actionsHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0)
          ),
        ),

        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: _topBarColor),
            borderRadius: BorderRadius.circular(8.0),
          ),
          margin: EdgeInsets.only(
            left: _actionsMargin,
            right: _actionsMargin,
            bottom: _actionsMargin,
          ),
          child: Center(
            child: Text(
              'Entendi.',
              style: TextStyle(
                  fontSize: 18.0
              ),
            ),
          ),
        )
    );
  }
}






class MailSubmitDialogBuilder extends CommonDialogBuilder {
  final Color _topBarColor = Colors.purple[300];
  final IconData iconData = IconData(57569, fontFamily: 'MaterialIcons');

  final double _contentHorizontalPadding = 24.0;

  final double _actionsMargin = 16.0;

  final double _buttonWidth = 116.0;

  //final String _title = 'Insira seu email:';

  @override
  Widget makeTopBarWidget() {
    return Container(
        height: topBarHeight,
        constraints: BoxConstraints(minWidth: 300),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
            color: _topBarColor),
        child: Icon(
          iconData,
          color: Colors.white,
          size:
          40, //THIS SHOULD NOT BE HARDCODED... CHANGE THIS TO SOMETHING MORE ADAPTIVE.
        ));
  }

  @override
  Widget makeTitleWidget() {
    return SizedBox(
        height: titleHeight
    );
  }

  @override
  Widget makeContentWidget() {
    return Container(
        height: contentHeight,
        padding: EdgeInsets.symmetric(horizontal: _contentHorizontalPadding),
        child: Column(
          children: <Widget>[


            Padding(
              padding: EdgeInsets.only(
                  bottom: 16.0
              ),
              child: Text(
                  'Email:',
                  style: TextStyle(
                      fontWeight: FontWeight.bold
                  )
              ),
            ),


            TextField(
              decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey[300],
                  hintText: 'Insira aqui seu email.'
              ),
            ),


          ],
        )

    );
  }

  @override
  Widget makeActionsWidget() {
    return Container(
        height: actionsHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0)
          ),
        ),

        child: Row(
          children: <Widget>[
            Container(
              width: _buttonWidth,
              decoration: BoxDecoration(
                border: Border.all(color: _topBarColor),
                borderRadius: BorderRadius.circular(8.0),
              ),
              margin: EdgeInsets.only(
                left: _actionsMargin,
                bottom: _actionsMargin,
              ),
              child: Center(
                child: Text(
                  'Cancelar',
                  style: TextStyle(
                      fontSize: 18.0
                  ),
                ),
              ),
            ),

            // THIS WAY OF SPACING THE BUTTONS IS VERY WRONG... FIX IT LATER.
            SizedBox(
              width: _actionsMargin,
            ),

            Container(
              width: _buttonWidth,
              decoration: BoxDecoration(
                border: Border.all(color: _topBarColor),
                borderRadius: BorderRadius.circular(8.0),
              ),
              margin: EdgeInsets.only(
                right: _actionsMargin,
                bottom: _actionsMargin,
              ),
              child: Center(
                child: Text(
                  'OK',
                  style: TextStyle(
                      fontSize: 18.0
                  ),
                ),
              ),
            ),
          ],
        )
    );
  }
}





class CommonDialogWidget extends StatelessWidget {
  final CommonDialogBuilder commonDialogBuilder;

  CommonDialogWidget(this.commonDialogBuilder);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: SizedBox(
        height: commonDialogBuilder.dialogHeight,
        child: Column(
          children: <Widget>[
            commonDialogBuilder.makeTopBarWidget(),
            commonDialogBuilder.makeTitleWidget(),
            commonDialogBuilder.makeContentWidget(),
            commonDialogBuilder.makeActionsWidget(),
          ],
        ),
      ),
    );
  }
}