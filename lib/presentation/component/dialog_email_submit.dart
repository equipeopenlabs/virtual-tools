import 'package:flutter/material.dart';
import 'package:virtual_tools/presentation/component/text_field.dart';
import 'package:virtual_tools/res/global_theme.dart';
import './dialog_template.dart';
import 'package:virtual_tools/res/colors.dart';

class EmailSubmitDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DialogTemplate(
      icon: IconData(57569, fontFamily: 'MaterialIcons'),
      children: <Widget>[
        Padding(
            padding: EdgeInsets.symmetric(vertical: 0.0),
            child: Center(
              child: Text(
                'Insira abaixo seu email:',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                    color: Colors.grey[700]),
              ),
            )),
        TextFieldApp(
          type: TextFieldApp.TYPE_TXT_FIELD_INPUT_DECORATION,
          text: 'Email',
          fieldKey: Key('emailFieldKey'),
        ),
        SizedBox(
          height: 32.0,
        ),
        GestureDetector(
          onTap: (){Navigator.pop(context);}, //added so it closes the dialog
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(
                    color: GlobalTheme.state.atributeTheme.primaryColor),
                borderRadius: BorderRadius.circular(10.0)),
            child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    'Enviar',
                    style: TextStyle(fontSize: 18.0),
                  ),
                )),
          ),
        ),
      ],
    );
  }
}
