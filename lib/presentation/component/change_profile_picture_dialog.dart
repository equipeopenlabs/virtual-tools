import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:virtual_tools/presentation/component/generic_dialog.dart';
import 'package:virtual_tools/presentation/profile_picture_provider.dart';

class ChangeProfilePictureDialog extends GenericDialog {
  //var _profilePictureProvider = ProfilePictureProvider();
  //File imageFile;

  ChangeProfilePictureDialog({@required List<Function> functions})
    : super(
        icon: IconData(58294, fontFamily: 'MaterialIcons'),
        barColor: Colors.blue[800],
        textColor: Colors.grey[700],
        title: 'Alterar foto de perfil',
        body: '',
        buttonText: ['Camera', 'Galeria'])
  {
    buttonFunctions = functions;
  }

  /*@override
  Widget build(BuildContext context) {
    /*buttonFunctions = [
      () async {
        imageFile = await _profilePictureProvider.getImageFromSource(ImageSource.camera);
        Navigator.of(context, rootNavigator: true).pop();
      },
      () async {
        imageFile = await _profilePictureProvider.getImageFromSource(ImageSource.gallery);
        Navigator.of(context, rootNavigator: true).pop();
      }
    ];*/

    return super.build(context);
  }*/
}