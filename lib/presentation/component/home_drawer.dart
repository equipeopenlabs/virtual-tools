import 'package:flutter/material.dart';

class HomeDrawer extends StatelessWidget {

  final String drawerImage;
  final Color colorLeft;
  final Color colorRight;
  final Function (int) onPressed;

  const HomeDrawer({Key key,
    @required this.drawerImage,
    @required this.colorLeft,
    @required  this.colorRight,
    @required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    BoxDecoration boxDecoration = new BoxDecoration(
      border: Border.all(color: Colors.white),
      borderRadius: BorderRadius.all(Radius.circular(10)),
      gradient: new LinearGradient(
        colors: [colorLeft, colorRight],
      ),
    );

    Container container(String titulo, String subtitulo, IconData icone, int index) => Container(
      decoration: boxDecoration,
      child: ListTile(
        title: Text(titulo),
        subtitle: Text(subtitulo),
        trailing: Icon(icone),
        onTap: (){
          onPressed(index);
        },
      ),
    );

    return Padding(
      padding: const EdgeInsets.only(left: 100.0),
      child: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              padding: EdgeInsets.all(0.0),
                child: Container(
                  decoration: new BoxDecoration(
                    //border: Border.all(color: Colors.white),
                    gradient: new RadialGradient(
                      radius: 1.5,
                      colors: [colorLeft, colorRight],
                    ),
                  ),
                  child: Image.asset(drawerImage),
                  height: double.infinity,
                  width: double.infinity,
                )
            ),

            //container("Título", "Subtítulo", Icons.list, 0),
            container("Home", "Subtítulo", Icons.home, 0),
            container("Settings", "Subtítulo", Icons.settings, 1),
            container("Users", "Subtítulo", Icons.account_circle, 2),
          ],
        ),
      ),
    );
  }
}
