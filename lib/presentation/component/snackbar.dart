import 'package:flutter/material.dart';

class GenericSnackBar {
  GlobalKey<ScaffoldState> scaffoldKey;
  String type;
  String text;
  // action
  //duration
  //animation
  //background color

  // predefined types
  static const String TYPE_LOGIN_SNACKBAR = "loginSnackBarError";

  GenericSnackBar(
      {@required this.scaffoldKey, @required this.type, this.text}) {
    switch (type) {
      case TYPE_LOGIN_SNACKBAR:
        _loginSnackBarError();
    }
  }

  void _loginSnackBarError() {
    showSnackBar(text);
  }

  void showSnackBar(String _message) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: Text(_message)));
  }
}
