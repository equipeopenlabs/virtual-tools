import 'package:flutter/material.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/utils/field_validators.dart';
import 'package:virtual_tools/res/global_theme.dart';

// ignore: must_be_immutable
class TextFieldApp extends StatelessWidget {
  static const String TYPE_TXT_FIELD_INPUT_DECORATION =
      "txtFieldInputDecoration";
  ThemeData theme;
  String type;
  String text;
  FocusNode fieldFocusId; // current field Id
  FocusNode nextFieldFocusId; // next field Id
  BuildContext parentContext; //  used for changing fields focus
  Function callbackFunction; // function to be called on the parent widget
  Function saveFieldCallbackFunction; //save function to be called on the parent widget
  Key fieldKey; // we use the fieldKey to set certain things like validator and onsave functions
  // known keys -> loginFieldKey , passwordFieldKey

  TextFieldApp({
    @required this.type,
    @required this.text,
    @required this.fieldKey,
    this.fieldFocusId,
    this.nextFieldFocusId,
    this.parentContext,
    this.callbackFunction,
    this.saveFieldCallbackFunction,
  });

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    return _managerTextField();
  }

  Widget _managerTextField() {
    switch (type) {
      case TYPE_TXT_FIELD_INPUT_DECORATION:
        return _txtFieldInputDecotation();
    }
  }

  Widget _txtFieldInputDecotation() {
    return new Theme(
      data: theme.copyWith(
          primaryColor: theme.primaryColor,
          hintColor: theme.primaryColor),
      child: new TextFormField(
        style: TextStyle(color: theme.cursorColor),
        // changes the enter button to next when there is another filed to focus, and to done when there isnt
        textInputAction: nextFieldFocusId != null
            ? TextInputAction.next
            : TextInputAction.done,
        focusNode: fieldFocusId,
        validator: (String textInput) {
          // chooses the validator according to the textFormField key
          if (fieldKey == Key('loginFieldKey'))
            return LoginFieldValidator.validate(textInput);
          else if (fieldKey == Key('passwordFieldKey'))
            return PwdFieldValidator.validate(textInput);
          else //  validator expects a String with the error message that is displayed behind the field
            return null; // or null if the field is valid
        },
        onSaved: (String textInput) {
          // called when the form calls save on the textFormFields
          saveFieldCallbackFunction(textInput);
        },
        onFieldSubmitted: (String textInput) {
          if (nextFieldFocusId != null)
            // if there is another field to focus change to it when I press enter
            FocusScope.of(parentContext).requestFocus(nextFieldFocusId);
          else if (callbackFunction != null)
            // on the last field run the callback function when I press enter
            callbackFunction();
        },
        decoration: new InputDecoration(
          labelText: text != null ? text : "",
          labelStyle:
              theme.textTheme.caption.copyWith(color: theme.primaryColor),
        ),
        obscureText: text == "Senha" ? true : false,
      ),
    );
  }
}
