import 'package:flutter/material.dart';

class GenericDialog {
  final IconData icon;
  final Color barColor;
  final Color textColor;
  final String title;
  final String body;
  final List<String> buttonText;

  List<Function> buttonFunctions;

  GenericDialog(
      {this.icon, this.barColor, this.textColor, this.title, this.body, this.buttonText});

  Widget buildColoredBarWithIcon() => Container(
    height: 64,
    constraints: BoxConstraints(minWidth: 300),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
        color: barColor),
    child: Icon(icon,
        //IconData(59693, fontFamily: 'MaterialIcons')
        color: Colors.white,
        size: 40),
  );

  Widget buildPaddedDialogMessage() => Padding(
    padding: EdgeInsets.symmetric(horizontal: 24.0),
    child: Text(
      body,
      style: TextStyle(color: textColor),
    ),
  );

  Widget buildButton(String text) => Container(
    decoration: BoxDecoration(
      border: Border.all(color: barColor),
      borderRadius: BorderRadius.circular(8.0),
    ),
    margin: EdgeInsets.all(16.0),
    child: Center(
      child: Text(
        text,
        style: TextStyle(color: textColor),
      ),
    ),
  );

  List<Widget> buildButtons(BuildContext context) {
    List<Widget> buttons = [];
    for (int i = 0; i < buttonText.length; i++) {
      buttons.add(
        Expanded(
          child: GestureDetector(
            child: Align(
              child: buildButton(buttonText[i]),
              alignment: Alignment.bottomCenter,
            ),
            onTap: buttonFunctions != null ? buttonFunctions[i] : null,
          ),
        ),
      );
    }
    return buttons;
  }

  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: SizedBox(
        height: 256,
        child: Column(
          children: <Widget>[
            buildColoredBarWithIcon(),
            Container(
              child: Text(title,
                  style:
                  TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0, color: textColor)),
              padding: EdgeInsets.symmetric(vertical: 16.0),
            ),
            buildPaddedDialogMessage(),
            Flexible(
              child: Row(
                children: buildButtons(context),
              ),
            )
          ],
        ),
      ),
    );
  }
}
