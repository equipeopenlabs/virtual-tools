import 'package:flutter/material.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/res/global_theme.dart';

class ButtonApp extends StatelessWidget {
  static const String TYPE_COMMON_BUTTON = "commonButton";
  String type;
  String text;
  ThemeData theme;
  Function onTap;

  ButtonApp({Key key, @required this.type, @required this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    // TODO: implement build
    return _managerText();
  }

  Widget _managerText() {
    switch (type) {
      case TYPE_COMMON_BUTTON:
        return _button();
    }
  }

  Widget _button() {
    return Material(
      elevation: 5, //creates shadow
      color: GlobalTheme.state.atributeTheme.primaryColor,
      borderRadius: BorderRadius.circular(25.0),
      child: InkWell(
        highlightColor: Color.alphaBlend(
                Colors.black26, GlobalTheme.state.atributeTheme.primaryColor)
            .withOpacity(0.4),
        splashColor: Color.alphaBlend(
                Colors.black26, GlobalTheme.state.atributeTheme.primaryColor)
            .withOpacity(0.6),
        borderRadius: BorderRadius.circular(25.0),
        onTap: onTap,
        child: new Container(
          width: 420.0,
          height: 45.0,
          alignment: FractionalOffset.center,
          child: new Text(
            text,
            style: new TextStyle(
              color: Colors.white,
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.3,
            ),
          ),
        ),
      ),
    );

    // return new Container(
    //   width: 420.0,
    //   height: 45.0,
    //   alignment: FractionalOffset.center,
    //   decoration: new BoxDecoration(
    //     //color: theme.primaryColor,
    //     borderRadius: new BorderRadius.all(const Radius.circular(25.0)),
    //     // boxShadow: <BoxShadow>[
    //     //  BoxShadow(
    //     //   color: Colors/*.black,*/.grey[600],
    //     //   blurRadius: 6.0, // has the effect of softening the shadow
    //     //   //spreadRadius: 1.0, // has the effect of extending the shadow
    //     //   offset: Offset(0.0,2.0),
    //     //   )
    //     //]
    //   ),
    //   child: new Text(
    //     text,
    //     style: new TextStyle(
    //       color: Colors.white,
    //       fontSize: 15.0,
    //       fontWeight: FontWeight.bold,
    //       letterSpacing: 0.3,
    //     ),
    //   ),
    // );
  }
}
