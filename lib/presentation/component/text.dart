import 'package:flutter/material.dart';
import 'package:virtual_tools/res/colors.dart';

class TextApp extends StatelessWidget{

  static const String TYPE_COMMON_TEXT = "text";
  String type;
  String text;
  ThemeData theme;

  TextApp({Key key, @required this.type, @required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    // TODO: implement build
    return _managerText();
  }

  Widget _managerText(){

    switch(type){

      case TYPE_COMMON_TEXT:
        return _commonText();

    }
  }

  Widget _commonText(){

    return new Text(
      text != null ? text : "",
      textAlign: TextAlign.center,
      overflow: TextOverflow.ellipsis,
      softWrap: true,
      style: new TextStyle(
          letterSpacing: 0.5,
          color: theme.primaryColor,
          fontSize: 12.0),
    );
  }
}