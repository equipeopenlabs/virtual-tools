import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';

import '../profile_picture_provider.dart';
import 'change_profile_picture_dialog.dart';

class AvatarPicture extends StatefulWidget {
  @override
  _AvatarPictureState createState() => _AvatarPictureState();
}

class _AvatarPictureState extends State<AvatarPicture> {
  BuildContext context;

  var _changeProfilePictureDialog;
  var _profilePictureProvider = ProfilePictureProvider();
  Future<String> _profilePicturePathFuture;
  File _imageFile;
  bool _wasImageChanged = false;

  void _showDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return _changeProfilePictureDialog.build(context);
        });
  }

  Future<String> _getProfilePicturePath() async {
    final documentsDirectory = (await getApplicationDocumentsDirectory()).path;

    String profilePicturePath = join(
      documentsDirectory,
      ProfilePictureProvider.PROFILE_PICTURE_FILENAME,
    );

    return profilePicturePath;
  }

  @override
  void initState() {
    super.initState();

    // Functions to be called by the dialog buttons
    List<Function> dialogFunctions = [
      () async {
        var imageFile = await _profilePictureProvider
            .getImageFromSource(ImageSource.camera);
        Navigator.of(context, rootNavigator: true)
            .pop(); // Pop the dialog from the stack
        setState(() {
          _imageFile = imageFile;
          _wasImageChanged = true;
        });
      },
      () async {
        var imageFile = await _profilePictureProvider
            .getImageFromSource(ImageSource.gallery);
        Navigator.of(context, rootNavigator: true).pop();
        setState(() {
          _imageFile = imageFile;
          _wasImageChanged = true;
        });
      }
    ];

    _changeProfilePictureDialog =
        ChangeProfilePictureDialog(functions: dialogFunctions);
    _profilePicturePathFuture = _getProfilePicturePath();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    //print('build !');

    // To be shown when there is no profile picture set
    Image defaultProfilePictureWidget = Image.asset(
      'assets/images/avatar1.png',
      fit: BoxFit.fill,
      height: 70,
      width: 70,
    );

    return GestureDetector(
      onTap: _showDialog,
      child: FutureBuilder<String>(
        future: _profilePicturePathFuture,
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            if (FileSystemEntity.typeSync('${snapshot.data}') !=
                FileSystemEntityType.notFound) {
              if (_wasImageChanged == false) {
                // Open the image file on the first time the widget is built
                imageCache.clear();
                _imageFile = File('${snapshot.data}');
              }
              return Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          border: Border.all(color: Colors.grey, width: 2)),
                      child: ClipOval(
                        child: Image.file(
                          _imageFile,
                          fit: BoxFit.cover,
                          height: 70.0,
                          width: 70.0,
                        ),
                      )),
                  Container(
                    height: 90.0,
                    width: 70.0,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.blue,
                        child: Image.asset(
                          'assets/images/camera_profile.png',
                          height: 20,
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return defaultProfilePictureWidget;
            }
          } else {
            return Text('Loading...'); // TODO: mudar isso pra um loader
          }
        },
      ),
    );
  }
}
