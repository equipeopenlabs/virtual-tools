import 'package:flutter/material.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/res/global_theme.dart';


class DialogTemplate extends StatelessWidget {

  //  THIS SHOULD BE CHANGED TO USE THE APP COLORS DEFINED ELSEWHERE
  Color topBarColor = GlobalTheme.state.atributeTheme.primaryColor;

  static const double topBarHeight = 56.0;
  static const double borderRadius = 8.0;
  static const double topBarIconSize = 40.0;
  static const double height = 256.0;
  static const double bodyPaddingVertical = 20.0;
  static const double bodyPaddingHorizontal = 16.0;

  final IconData icon;
  final List<Widget> children;

  DialogTemplate({
    @required this.icon,
    @required this.children
  });

  Widget _topBar() => Container(
      height: topBarHeight,
      constraints: BoxConstraints(minWidth: double.infinity),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(borderRadius),
              topRight: Radius.circular(borderRadius)
          ),
          color: topBarColor
      ),
      child: Icon(
        icon,
        color: Colors.white,       // THIS LINE IS WRONG... SHOULD TAKE COLOR FROM CONTEXT THEME.
        size: topBarIconSize
      )
  );


  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: _topBar(),
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.symmetric(
        vertical: bodyPaddingVertical,
        horizontal: bodyPaddingHorizontal
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderRadius)),
      children: children,
    );
  }
}