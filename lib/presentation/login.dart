import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/services.dart';
//import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:virtual_tools/presentation/component/button.dart';
//import 'package:virtual_tools/presentation/component/dialog_template.dart';
import 'package:virtual_tools/presentation/component/snackbar.dart';
import 'package:virtual_tools/presentation/component/text.dart';
import 'package:virtual_tools/presentation/component/text_field.dart';
import 'package:virtual_tools/presentation/item/first_page.dart';
import 'package:virtual_tools/presentation/loginAnimation.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/res/global_theme.dart';
import 'package:virtual_tools/presentation/component/dialog_email_submit.dart';

class Login extends StatefulWidget {
  final String imagemLogin;
  final String imagemLogo;

  const Login({Key key, this.imagemLogin, this.imagemLogo}) : super(key: key);

  @override
  _LoginState createState() {
    // TODO: implement createState
    return new _LoginState();
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class _LoginState extends State<Login> with TickerProviderStateMixin {
  AnimationController _loginButtonController;
  var animationStatus = 0;
  FocusNode loginFieldFocus = new FocusNode();
  FocusNode pwdFieldFocus = new FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey =
      new GlobalKey<ScaffoldState>(); // necessary to use showSnackBar
  String _login;
  String _password;
  ThemeData theme;
  //String operadora;
  final operadora = GlobalTheme.state.operadora;

  @override
  void initState() {
    super.initState();
    _loginButtonController = new AnimationController(
        duration: new Duration(milliseconds: 3000), vsync: this);
  }

  @override
  void dispose() {
    _loginButtonController.dispose();
    loginFieldFocus.dispose();
    pwdFieldFocus.dispose();
    super.dispose();
  }

  void saveLoginFieldCallbackFunction(String textInput) {
    _login = textInput;
  }

  void savePasswordFieldCallbackFunction(String textInput) {
    _password = textInput;
  }

  void validateFormInputs() {
    if (_formKey.currentState.validate()) {
      // if fields are  valid save then and play the animation
      _formKey.currentState.save();
      setState(() {
        animationStatus = 1;
      });
      _playAnimation();
    } else // if fields are not valid calls the snackbar to inform the error
      GenericSnackBar(
          scaffoldKey: _scaffoldKey,
          text: ' Login e/ou senha inválido',
          type: GenericSnackBar.TYPE_LOGIN_SNACKBAR);
  }

  Future<Null> _playAnimation() async {
    try {
      await _loginButtonController.forward();
      await _loginButtonController.reverse();
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom]);
    theme = Theme.of(context);
    //FlutterStatusbarManager.setColor(Colors.transparent);
    timeDilation = 0.4;
    return (new WillPopScope(
      child: Container(
        // container contains the scaffold so that we can draw the wallpaper without it
        // compressing when the keyboard is called
        decoration: BoxDecoration(
          image: DecorationImage(
            image: ExactAssetImage(widget.imagemLogin),
            fit: BoxFit.fill,
          ),
        ),
        child: Scaffold(
          key: _scaffoldKey, // necessary to call the snackbar
          backgroundColor: Colors
              .transparent, // set transparent so it doesnt draws above the BoxDecoration

          body: Form(
            key:
                _formKey, // necessary to call validation and save methods on fields
            child: new Container(
              child: ScrollConfiguration(
                behavior: MyBehavior(),
                child: new ListView(
                  //padding: const EdgeInsets.all(0.0), //used on listview
                  children: <Widget>[
                    new Stack(
                      alignment: AlignmentDirectional.bottomCenter,
                      children: <Widget>[
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.only(
                                  left: 0,
                                  top: 150,
                                  right: 0,
                                  bottom: 0,
                                ),
                                child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.15,
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  child: Image.asset(
                                    widget.imagemLogo,
                                    fit: BoxFit.scaleDown,
                                  ),
                                )),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 20,
                                top: 10,
                                right: 20,
                                bottom: 0,
                              ),
                              child: Container(
                                child: new Align(
                                  alignment: Alignment.topCenter,
                                  child: new TextFieldApp(
                                    fieldKey: Key('loginFieldKey'),
                                    type: TextFieldApp
                                        .TYPE_TXT_FIELD_INPUT_DECORATION,
                                    text: "Login",
                                    parentContext: context,
                                    fieldFocusId: loginFieldFocus,
                                    nextFieldFocusId: pwdFieldFocus,
                                    saveFieldCallbackFunction:
                                        saveLoginFieldCallbackFunction,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 20,
                                top: 10,
                                right: 20,
                                bottom: 0,
                              ),
                              child: Container(
                                child: new Align(
                                  alignment: Alignment.topCenter,
                                  child: new TextFieldApp(
                                    fieldKey: Key('passwordFieldKey'),
                                    type: TextFieldApp
                                        .TYPE_TXT_FIELD_INPUT_DECORATION,
                                    text: "Senha",
                                    fieldFocusId: pwdFieldFocus,
                                    callbackFunction: validateFormInputs,
                                    saveFieldCallbackFunction:
                                        savePasswordFieldCallbackFunction,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 80,
                            ),
                            FlatButton(
                              // highlightColor: Colors.transparent,
                              // splashColor: Colors.transparent,
                              // padding: const EdgeInsets.only(
                              //   top: 100, //100.0
                              // ),

                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return EmailSubmitDialog();
                                    });
                              },
                              child: new TextApp(
                                type: TextApp.TYPE_COMMON_TEXT,
                                text: "Esqueceu sua senha?",
                              ),
                            ),
                          ],
                        ),
                        animationStatus == 0
                            ? new Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 55.0,
                                    right: 20,
                                    left: 20), //40,20,20
                                child: new ButtonApp(
                                  type: ButtonApp.TYPE_COMMON_BUTTON,
                                  text: "Entrar",
                                  onTap: validateFormInputs,
                                ),
                              )
                            : new StaggerAnimation(
                                buttonController: _loginButtonController.view),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
