import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
//import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:virtual_tools/presentation/generic.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/presentation/component/button.dart';

class ProductDetails extends StatefulWidget {

  @override
  _ProductDetails createState() => new _ProductDetails();
}

const kExpandedHeight = 300.0;

class _ProductDetails extends State<ProductDetails> with Generic {

  String result = "Hey there !";
  ThemeData theme;

  _ProductDetails();

  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();

    _scrollController = ScrollController()
      ..addListener(() => setState(() {}));
  }

  bool get _showTitle {
    return _scrollController.hasClients &&
        _scrollController.offset > kExpandedHeight - kToolbarHeight;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom],);
        theme = Theme.of(context);
    //FlutterStatusbarManager.setColor(Colors.transparent);
    return Scaffold(
        body: NestedScrollView(
            headerSliverBuilder: (BuildContext context,
                bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  expandedHeight: 225.0,
                  floating: false,
                  pinned: true,
                  backgroundColor: theme.primaryColor,
                  flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      title: Text("Open Labs",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          )),
                      background: Image.asset(
                        'assets/images/logo-circle-open-labs.png', width: 40, height: 40,
                      )),
                )
                ];
            },
            body: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    child: new Stack(children: <Widget>[
                      new Align(
                        alignment: Alignment.topLeft,
                        child: new Text("TESTE",
                            style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold)),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(
                            left: 0,
                            top: 25,
                            right: 0,
                            bottom: 0,
                          ),
                          child: Container(
                              child: new Align(
                                alignment: Alignment.center,
                                child: new Text(
                                    "Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs Open Labs .",
                                    textAlign: TextAlign.center,
                                    ),
                              )
                          )
                      ),

                    ]),
                  ),

                  Container(
                    padding: EdgeInsets.all(20),
                    
                   // decoration: new BoxDecoration(
                     // borderRadius: new BorderRadius.circular(1.0),
                    //  boxShadow:<BoxShadow>[
                    //    BoxShadow(
                    //      color: Colors.black,
                    //      blurRadius: 10.0, // has the effect of softening the shadow
                    //      spreadRadius: 1.0, // has the effect of extending the shadow
                    //      offset: Offset(
                    //          2.0, // horizontal, move right 10
                    //          2.0,
                    //    ),
                     //   )
                    //    ],
                    //),
                    child: new ButtonApp(
                          type: ButtonApp.TYPE_COMMON_BUTTON,
                          text: "Realizar Diagnótico"),
                      
                    
                  )
                ])
        )
    );
  }
}
