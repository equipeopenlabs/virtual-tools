import 'package:flutter/material.dart';
import 'package:virtual_tools/res/colors.dart';
import 'package:virtual_tools/presentation/home.dart';
import 'package:virtual_tools/res/global_theme.dart';

abstract class Generic {
  static Home home;

  PreferredSize commonAppBar(String title) {
    return PreferredSize(
      preferredSize: Size.fromHeight(66.0),
      child: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Material(
            color: Colors.white,
            borderRadius: BorderRadius.circular(50),
            elevation: 2,
            child: Container(
              width: 45,
              height: 45,
              decoration: BoxDecoration(
                  //color: Colors.red,
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.fitHeight,
                      image: AssetImage(GlobalTheme.state.atributeTheme.avatarLogo)
                          )
                          ),
            ),
            // child: Image.asset(
            //   GlobalTheme.state.atributeTheme.avatarLogo,
            //   /*'assets/images/logo-circle-open-labs.png',*/
            //   width: 45,
            //   height: 45,
            //   fit: BoxFit.contain,
            // ),
          ),
        ),
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Padding(
            padding: const EdgeInsets.only(top: 36, left: 70),
            child: Row(
              children: <Widget>[
                Column(
                  //mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text("Olá, Amiguinho",
                        style: TextStyle(
                          fontFamily: "fonts/dosis-bold.ttf",
                          fontSize: 12,
                          //color: ColorsApplication.formatColorBase()
                        )),
                    Row(
                      children: <Widget>[
                        Text("Contrato: 123456",
                            style: TextStyle(
                                fontFamily: "fonts/dosis-regular.ttf",
                                fontSize: 11,
                                color: Colors.white)),
                        Icon(
                          Icons.expand_more,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
