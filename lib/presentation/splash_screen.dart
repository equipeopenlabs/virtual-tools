import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:virtual_tools/presentation/item/itens_grid.dart';

class SplashScreen extends StatefulWidget{
  final String imagemSplash;

  const SplashScreen({Key key, this.imagemSplash}) : super(key: key);

  @override
  _SplashScreen createState() => new _SplashScreen();
}

class _SplashScreen extends State<SplashScreen> with TickerProviderStateMixin{

  AnimationController controller;
  Animation<double> animation;
  bool _visible = false;

  @override
  Widget build(BuildContext context) {

    SystemChrome.setEnabledSystemUIOverlays([]);

    return new Scaffold(

      backgroundColor: Colors.white,
      body: new Center(
        child: Container(
          child: AnimatedOpacity(
            opacity: _visible ? 1.0 : 0.0,
            duration: Duration(milliseconds: 1500),
            child: new Image.asset(widget.imagemSplash, width: 60, height: 60),
          ),
        ),
      )
    );
  }

  @override
  void initState() {

    super.initState();
    startAnimationTimer();
  }

  startAnimationTimer() async{

    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, _animationFade);
  }

  startSplashScreenTimer() async{

    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, _navigationNextPage);
  }

  _animationFade(){

    setState(() {
      _visible = true;
    });
    startSplashScreenTimer();
  }

  _navigationNextPage(){

    Navigator.of(context).pushReplacementNamed('/Login');
    //var item = ItemGrid.list[2];
  }
}