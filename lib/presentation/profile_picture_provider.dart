import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePictureProvider {
  static const String PROFILE_PICTURE_FILENAME = 'profile_picture.png';

  Future<File> getImageFromSource(ImageSource source) async {
    // Get file from image picker
    var imageFile = await ImagePicker.pickImage(source: source);

    final documentsDirectory = (await getApplicationDocumentsDirectory()).path;

    final path = join(
      documentsDirectory,
      PROFILE_PICTURE_FILENAME,
    );

    //imageCache.clear();

    // Copy the temp file to the permanent file path
    if (imageFile != null) {
      await imageFile.copy('$path');
    }

    return imageFile;
  }
}