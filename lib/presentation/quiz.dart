import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'dart:async';
import 'package:flutter/material.dart';

class QuizState extends StatelessWidget {
  _QuizState _quizState;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Pesquisa de satisfação",
      debugShowCheckedModeBanner: false,
      home: Quiz(),
    );
  }
}

class Quiz extends StatefulWidget {
  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  // criando instância do FlutterWebViewPlugin
  final flutterWebViewPlugin = FlutterWebviewPlugin();

  StreamSubscription<String> _onUrlChanged;

  @override
  void initState() {
    super.initState();

    _onUrlChanged = flutterWebViewPlugin.onUrlChanged.listen((String url) {
      if (url != "https://pesquisa-open.herokuapp.com/?exampleRadios=opcao1") {
        print(' $url  ');
        Navigator.pop(context);
        flutterWebViewPlugin.close();
         Navigator.pushNamed(context, '/Sucess');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: "https://pesquisa-open.herokuapp.com/?exampleRadios=opcao1",
      appBar: AppBar(
        title: Text("Pesquisa"),
      ),
      withZoom: false,
      withLocalStorage: true,
      hidden: true,
      initialChild: Center(
          //loading
          child: Container(
              height: 100,
              width: 100,
              child: Image.asset(
                "assets/gifs/loading.gif",
              ))),
    );
  }
}
